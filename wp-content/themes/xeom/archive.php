<?php
    get_header();    
    $xeom_archive_layout = get_theme_mod('xeom_archive_layout', true);
    $xeom_disable_archive_sidebar = get_theme_mod('xeom_disable_archive_sidebar', true);
    if ( $xeom_disable_archive_sidebar ) {
        $xeom_col_md = 12;
    } else {
        $xeom_col_md = 9;
    }
?>
<div class="container">
    <div id="main">
        <div class="archive-box">
		<?php if ( is_category() ) : ?>
            <span><?php esc_html_e('Browsing Category', 'xeom'); ?>: </span>
		    <h3><?php printf( __('%s', 'xeom'), single_cat_title('', false) ); ?></h3>
        <?php elseif ( is_tag() ) : ?>
            <span><?php esc_html_e('Browsing Tag', 'xeom'); ?>: </span>
			<h3><?php printf(__('%s', 'xeom'), single_tag_title('', false)); ?></h3>
        <?php elseif ( is_author() ) : ?>
            <span><?php esc_html_e('All Posts By', 'xeom'); ?>: </span>
			<h3><?php the_post(); echo get_the_author(); ?></h3>
        <?php else : ?>
			<?php if ( is_day() ) : ?>
			<span><?php esc_html_e('Daily Archives', 'xeom'); ?>: </span>
            <h3><?php echo get_the_date(); ?></h3>            
            <?php elseif ( is_month() ) : ?>
            <span><?php esc_html_e('Monthly Archives', 'xeom'); ?>: </span>
            <h3><?php echo get_the_date( _x( 'F Y', 'monthly archives date format', 'xeom' ) ); ?></h3>            
            <?php elseif ( is_year() ) : ?>
            <span><?php esc_html_e('Yearly Archives', 'xeom'); ?>: </span>
			<h3><?php echo get_the_date( _x( 'Y', 'yearly archives date format', 'xeom' ) ); ?></h3>			
            <?php else : ?>
                    <h3><?php esc_html_e('Archives', 'xeom'); ?>: </h3>
			<?php endif; ?>
        <?php endif; ?>
		</div>
        <div class="row">
            <div class="col-md-<?php echo esc_attr($xeom_col_md); ?>">
                <?php if ( $xeom_archive_layout == 'list' ) : ?>
                    <?php get_template_part('loop/blog', 'list'); ?>
                <?php elseif ( $xeom_archive_layout == 'masonry' ) : ?>
                    <?php get_template_part('loop/blog', 'masonry'); ?>
                <?php elseif ( $xeom_archive_layout == 'zigzag' ) : ?>
                    <?php get_template_part('loop/blog', 'zigzag'); ?>
                <?php else : ?>
                    <?php get_template_part('loop/blog', 'standard'); ?>
                <?php endif; ?>
            </div>
            <?php if ( !$xeom_disable_archive_sidebar ) : ?>
            <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
            <?php endif; ?>
        </div>
    </div>    
</div>
<?php get_footer(); ?>