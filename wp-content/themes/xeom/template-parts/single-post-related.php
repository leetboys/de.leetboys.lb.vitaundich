<?php 
$orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) :
	$category_ids = array();
	foreach($categories as $individual_category) {
        $category_ids[] = $individual_category->term_id;
	}
	$args = array(
		'category__in'        => $category_ids,
		'post__not_in'        => array($post->ID),
		'posts_per_page'      => 3,
		'ignore_sticky_posts' => 1,
		'orderby'             => 'rand'
	);
	$new_query = new wp_query( $args );
?>
    <?php if( $new_query->have_posts() ) : ?>
    <div class="post-related">
        <div class="post-box"><h4 class="post-box-title"><span><?php esc_html_e('Das könnte ihnen auch gefallen', 'xeom'); ?></span></h4></div>
            <div class="row">
            <?php while( $new_query->have_posts() ) : $new_query->the_post(); ?>
                <div class="col-md-4">
    				<?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) : ?>
                    <?php
                        $thumb = xeom_resize_image( get_post_thumbnail_id() , wp_get_attachment_thumb_url(), 450, 300, true, false );
                        if ( $thumb['url'] ) : ?>
    				<a href="<?php echo get_permalink() ?>">
                        <img src="<?php echo esc_url($thumb['url']); ?>" alt=""/>
                    </a>
                        <?php endif; ?>
    				<?php endif; ?>					
    				<h4 class="post-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
    				<time><a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ); ?> "><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php the_time(get_option('date_format')); ?></a></time>
                </div>
            <?php endwhile; ?>
    		</div>
        </div>
    <?php endif; ?>
<?php endif;
$post = $orig_post;
wp_reset_query();