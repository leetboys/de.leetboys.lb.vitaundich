<?php
    global $wp_query;
	$number             = get_theme_mod( 'xeom_featured_slider_slides' );
    $featured_cat       = get_theme_mod( 'xeom_featured_cat' );
	$get_featured_posts = get_theme_mod('xeom_featured_id');	
	
	if($get_featured_posts) {
		$featured_posts = explode(',', $get_featured_posts);
		$args = array( 'showposts' => $number, 'post_type' => array('post', 'page'), 'post__in' => $featured_posts, 'orderby' => 'post__in' );
        $query_args = array(
            'post_type' => 'post',
            'paged' => $paged,
            'post__not_in' => $featured_posts,
            'ignore_sticky_posts' => 1
        );
	} else {
		$args = array( 'cat' => $featured_cat, 'showposts' => $number );
        $query_args = array(
            'post_type' => 'post',
            'paged' => $paged,
            'category__not_in' => array( (int)$featured_cat ),
            'ignore_sticky_posts' => 1
        );
	}
    
    $wp_query   = new WP_Query( $query_args );
    
    $feat_query = new WP_Query( $args );
    
    if ($feat_query->have_posts()) : ?>
    <div class="featured-area container">
       <div class="feature-slider" data-autoplaytimeout="<?php echo get_theme_mod('xeom_featured_slider_autoplayTimeout'); ?>" data-autoplay="<?php echo get_theme_mod('xeom_featured_slider_auto_play'); ?>">
    <?php
        while ($feat_query->have_posts()) :
            $feat_query->the_post();
            $image_featured = xeom_resize_image( get_post_thumbnail_id($post->ID) , wp_get_attachment_thumb_url(), 720, 482, true, true );
            $image_featured = $image_featured['url']; ?>
        	<div class="slide-item row">
                <div class="slide-item-image col-md-8">
                    <a href="<?php echo get_permalink(); ?>"><figure><img src="<?php echo esc_url($image_featured); ?>" alt=""/></figure></a>
                </div>
        		<div class="slide-item-text col-md-4">
        			<div class="post-text-inner">
                        <p class="post-cats"><i class="fa fa-bookmark-o"></i>&nbsp;&nbsp;<?php the_category(', '); ?></p>
            			<h4 class="post-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <div class="post-excerpt">
                            <?php xeom_the_excerpt_max_charlength(560); ?>
                        </div>
                    </div>
        		</div>
        	</div><?php
        endwhile; ?>    
        </div>
    </div><?php
    endif;
?>