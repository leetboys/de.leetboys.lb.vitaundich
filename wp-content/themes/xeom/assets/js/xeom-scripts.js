(function($){
	"use strict";
    $(document).ready(function() {
        $('.top-search .icon-search').click( function(){
            $('.top-search .searchform').toggle(100);
        });

        if ( $('.post').length ) { $('.post').fitVids(); }
        
        if ( $('select').length ) { $('select').chosen(); }
        
        if ( $('.slider').length )
        {
            $('.slider').owlCarousel({
        		items:1,
        		nav:true,
        		dots:false,
        		navText:['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>']            
        	});
        }
        
        if ( $('.feature-slider').length )
        {
            var $autoplay = $('.feature-slider').data('autoplay') == '1' ? true : false;
            var $autoplayTimeout = ( parseInt( $('.feature-slider').data('autoplaytimeout') ) > 0 ) ? parseInt( $('.feature-slider').data('autoplaytimeout') ) : 5000;
            $('.feature-slider').owlCarousel({
        		items:1,
        		nav:true,
        		dots:false,
                loop: true,
                autoplay: $autoplay,
                autoplayTimeout: $autoplayTimeout,
        		navText:['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>']
        	});
        }
        
        if ( $('.togole-mainmenu').length ) {
            $('.togole-mainmenu').click( function(){
                $('#nav-wrapper').toggle(350);
            } );
        }
        
        $('#nav-wrapper .menu .caret').click( function() {
            var $submenu = $(this).closest('.menu-item-has-children').find(' > .sub-menu');
            
            if ( $submenu.hasClass('az-active') ) {
                $submenu.removeClass('az-active');
            } else {
                $submenu.addClass('az-active');
            }
            
            return false;
        });
        
       
        
    });
})(jQuery);

var $ = jQuery.noConflict();
function xeom_menu_sticky() {
    if ( $('#main-menu').hasClass('is_ticky_nav') )
    {
        var $nav_top =  ( $("#wpadminbar").length ) ? $('#wpadminbar').innerHeight() : 0;
        $('#main-menu').css({
           'top': $nav_top
        });
        
        $('.az-header-wrapper').css({
           'margin-top': $('#main-menu').innerHeight()
        });
        
    }
}

// Masonry Blog
var $blog_masonry = $('.az-blog-masonry');
function initGrid()
{    
    var t = $blog_masonry.attr("data-cols");
    if (t == "1") {
        var n = $blog_masonry.width();
        var r = 1;
        return r
    }
    if (t == "2") {
        var n = $blog_masonry.width();
        var r = 2;
        if (n < 480) r = 1;
        return r
    } else if (t == "3") {
        var n = $blog_masonry.width();
        var r = 3;
        if (n < 480) r = 1;
        else if (n >= 480 && n < 788) r = 2;
        else if (n >= 788 && n < 1160) r = 3;
        else if (n >= 1160) r = 3;
        return r
    } else if (t == "4") {
        var n = $blog_masonry.width();
        var r = 4;
        if (n < 480) r = 1;
        else if (n >= 480 && n < 788) r = 2;
        else if (n >= 788 && n < 1160) r = 3;
        else if (n >= 1160) r = 4;
        return r
    } else if (t == "5") {
        var n = $blog_masonry.width();
        var r = 5;
        if (n < 480) r = 1;
        else if (n >= 480 && n < 788) r = 2;
        else if (n >= 788 && n < 1160) r = 3;
        else if (n >= 1160) r = 5;
        return r
    } else if (t == "6") {
        var n = $blog_masonry.width();
        var r = 5;
        if (n < 480) r = 1;
        else if (n >= 480 && n < 788) r = 2;
        else if (n >= 788 && n < 1160) r = 3;
        else if (n >= 1160) r = 6;
        return r
    } else if (t == "7") {
        var n = $blog_masonry.width();
        var r = 5;
        if (n < 480) r = 1;
        else if (n >= 480 && n < 788) r = 2;
        else if (n >= 788 && n < 1160) r = 3;
        else if (n >= 1160) r = 8;
        return r
    }
}

function callWidthGridItem()
{
    if ( $blog_masonry.length )
    {
        var t = initGrid();
        var n = $blog_masonry.width();
        var r = n / t;
        r = Math.floor(r);
        $blog_masonry.find('.post').each(function (t) {
            $(this).css({ width: r + "px" });
        });
    
        $blog_masonry.masonry({ itemSelector: '.post' });
    }
    
}

callWidthGridItem();

$(window).load(function () {
    setTimeout( function() {
       callWidthGridItem(); 
    }, 2000);
    xeom_menu_sticky();
});

$(window).resize(function(){
    callWidthGridItem();
    xeom_menu_sticky();
});