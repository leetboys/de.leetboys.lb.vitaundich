<?php
if ( function_exists('xeom_require_file') )
{
    // Load Classes
    xeom_require_file( XEOM_CORE_CLASSES . 'class-tgm-plugin-activation.php' );
    xeom_require_file( XEOM_CORE_CLASSES . 'wp_bootstrap_navwalker.php' );
    xeom_require_file( XEOM_CORE_CLASSES . 'like-post/xeom-like-post.php' );
    
    // Load Functions
    xeom_require_file( XEOM_CORE_FUNCTIONS . 'customizer/xeom_custom_controll.php' );
    xeom_require_file( XEOM_CORE_FUNCTIONS . 'customizer/xeom_customizer_settings.php' );
    xeom_require_file( XEOM_CORE_FUNCTIONS . 'customizer/xeom_customizer_style.php' );
    xeom_require_file( XEOM_CORE_FUNCTIONS . 'xeom_resize_image.php' );
    
    // Load Widgets
    xeom_require_file( XEOM_CORE_WIDGETS . 'xeom_about_widget.php' );
    xeom_require_file( XEOM_CORE_WIDGETS . 'xeom_categories_images_widget.php' );
    xeom_require_file( XEOM_CORE_WIDGETS . 'xeom_latest_posts_widget.php' );
    xeom_require_file( XEOM_CORE_WIDGETS . 'xeom_social_widget.php' );
}