<?php
// Customizer - Add CSS --------------------------------------------------------------------------------------------
add_action( 'wp_head', 'aztheme_customizer_css' );
function aztheme_customizer_css() {
    ?>
    <style type="text/css">              
        <?php if ( get_theme_mod('xeom_accent_color') ) : ?>
            a {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            a:hover, a:focus {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .socials .social-icon:hover {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .feature-slider .owl-controls .owl-prev:hover, .feature-slider .owl-controls .owl-next:hover {
                background-color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .widget li a:hover {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .xeom_social_widget .social-widget a:hover, .post .post-share .social-icon:hover, .az-post-author .social-share a:hover {
                background: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .error-page .searchform button[type="submit"]:hover, .mc4wp-form input[type="submit"]:hover, #comment_submit:hover, .wpcf7-form-control.wpcf7-submit:hover {
                background: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .widget_tag_cloud a:hover, .az-post-tags a:hover {
                background-color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .post-cats {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .post-cats > a {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .post .post-title > a:hover, .post-text-inner .post-title > a:hover {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .slider .owl-nav > div:hover{
                background-color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .post-related .post-title a:hover {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            #commentform .submit:hover {
                background: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .az-pagination a, .az-single-post-next-prev a {
                background: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            .az-pagination a:hover, .az-single-post-next-prev a:hover {
                background-color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
                border-color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
            #az-footer .widget .latest-post a:hover {
                color: <?php echo get_theme_mod('xeom_accent_color'); ?>;
            }
        <?php endif; ?>

        <?php if ( get_theme_mod('xeom_custom_css') ) : ?>
            <?php echo get_theme_mod('xeom_custom_css'); ?>
        <?php endif; ?>
    </style>
    <?php
}