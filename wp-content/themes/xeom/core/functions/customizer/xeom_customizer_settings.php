<?php
function danny_customizer_style()
{
	wp_enqueue_style('customizer-css', get_stylesheet_directory_uri() . '/core/functions/customizer/css/customizer.css');
}
add_action('customize_controls_print_styles', 'danny_customizer_style');

function xeom_sanitize_default( $value ) {
	return $value;
}

/** AZTheme - Customizer - Add Settings */
function aztheme_register_theme_customizer( $wp_customize )
{
	/** Add Sections -----------------------------------------------------------------------------------------------------------*/
	$wp_customize->add_section( 'aztheme_new_section_general', array(
   		'title'        => 'General Settings',
   		'description'  => null,
   		'priority'     => 1
	) );
    $wp_customize->add_section( 'aztheme_new_section_header', array(
   		'title'        => 'Logo upload',
   		'description'  => null,
   		'priority'     => 2
	) );
    $wp_customize->add_section( 'aztheme_new_section_featured' , array(
		'title'       => 'Featured Area Settings',
		'description' => '',
		'priority'    => 3
	) );
    $wp_customize->add_section( 'aztheme_new_section_social_media', array(
   		'title'        => 'Social Media Settings',
   		'description'  => 'Enter your social media usernames. Icons will not show if left blank.',
   		'priority'     => 4
	) );
    $wp_customize->add_section( 'aztheme_new_section_footer', array(
   		'title'        => 'Footer Settings',
   		'description'  => '',
   		'priority'     => 5
	) );
    $wp_customize->add_section( 'aztheme_new_section_color_accent', array(
   		'title'        => 'Colors: Accent',
   		'description'  => '',
   		'priority'     => 6
	) );
    $wp_customize->add_section( 'aztheme_new_section_custom_css', array(
   		'title'        => 'Custom CSS',
   		'description'  => 'Add your custom CSS which will overwrite the theme CSS',
   		'priority'     => 7
	) );

    /** Add Settings ------------------------------------------------------------------------------------------------------------*/
    // General
    $wp_customize->add_setting('xeom_homepage_layout', array(
        'default'           => 'zigzag',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_archive_layout', array(
        'default'           => 'masonry',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_homepage_sidebar', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_archive_sidebar', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_single_post_sidebar', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_top_social', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_search_menu', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting('xeom_disable_is_sticky_menu', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    // Featured area
	$wp_customize->add_setting( 'xeom_featured_slider', array(
        'default' => false,
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
	$wp_customize->add_setting( 'xeom_featured_cat', array(
        'default' => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
	$wp_customize->add_setting( 'xeom_featured_id', array(
        'default' => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
	$wp_customize->add_setting( 'xeom_featured_slider_slides', array(
        'default' => '5',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_featured_slider_auto_play', array(
        'default' => true,
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_featured_slider_autoplayTimeout', array(
        'default' => '5000',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    
    // Header and Logo
    $wp_customize->add_setting( 'xeom_logo', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_logo_footer', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    // Social media settings
    $wp_customize->add_setting( 'xeom_facebook', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_twitter', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_instagram', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_pinterest', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_tumblr', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_bloglovin', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_tumblr', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_google', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_youtube', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_dribbble', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_soundcloud', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );    
    $wp_customize->add_setting( 'xeom_vimeo', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_linkedin', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    // Accent
    $wp_customize->add_setting( 'xeom_accent_color', array(
        'default'           => '#f37e7e',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );    
    // Footer
    $wp_customize->add_setting( 'xeom_footer_disable_social', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_footer_copyright', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    $wp_customize->add_setting( 'xeom_logo_footer', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    // Custom CSS
	$wp_customize->add_setting( 'xeom_custom_css', array(
        'default'           => '',
        'sanitize_callback' => 'xeom_sanitize_default'
    ) );
    
    /** Add Constrol ------------------------------------------------------------------------------------------------------------*/
    // General Settings
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'home_layout',
			array(
				'label'          => 'Homepage Layout',
				'section'        => 'aztheme_new_section_general',
				'settings'       => 'xeom_homepage_layout',
				'type'           => 'radio',
				'priority'	     => 1,
				'choices'        => array(
					'standard' => 'Standard Blog Layout',
					'zigzag' => 'Zigzag Blog Layout',
					'list' => 'List Blog Layout',
					'masonry' => 'Masonry Blog Layout'
				)
			)
		)
	);
    
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'archive_layout',
			array(
				'label'          => 'Archive Layout',
				'section'        => 'aztheme_new_section_general',
				'settings'       => 'xeom_archive_layout',
				'type'           => 'radio',
				'priority'	     => 2,
				'choices'        => array(
					'standard' => 'Standard Blog Layout',
					'zigzag' => 'Zigzag Blog Layout',
					'list' => 'List Blog Layout',
					'masonry' => 'Masonry Blog Layout'
				)
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'disable_homepage_sidebar',
			array(
				'label'      => 'Disable Sidebar on Hompage',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_homepage_sidebar',
				'type'		 => 'checkbox',
				'priority'	 => 3
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'disable_archive_sidebar',
			array(
				'label'      => 'Disable Sidebar on Archive',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_archive_sidebar',
				'type'		 => 'checkbox',
				'priority'	 => 4
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'disable_single_post_sidebar',
			array(
				'label'      => 'Disable Sidebar on Single Post',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_single_post_sidebar',
				'type'		 => 'checkbox',
				'priority'	 => 5
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'top_social_media',
			array(
				'label'      => 'Disable social media on header',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_top_social',
				'type'		 => 'checkbox',
				'priority'	 => 6
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'menu_disable_search',
			array(
				'label'      => 'Disable search on the menu',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_search_menu',
				'type'		 => 'checkbox',
				'priority'	 => 7
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'disable_is_sticky_menu',
			array(
				'label'      => 'Disable sticky menu ?',
				'section'    => 'aztheme_new_section_general',
				'settings'   => 'xeom_disable_is_sticky_menu',
				'type'		 => 'checkbox',
				'priority'	 => 8
			)
		)
	);
    // Logo
    $wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'upload_logo',
			array(
				'label'      => 'Upload logo top',
				'section'    => 'aztheme_new_section_header',
				'settings'   => 'xeom_logo',
				'priority'	 => 1
			)
		)
	);
    // Featured area
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'featured_slider',
			array(
				'label'      => 'Enable Featured Slider',
				'section'    => 'aztheme_new_section_featured',
				'settings'   => 'xeom_featured_slider',
				'type'		 => 'checkbox',
				'priority'	 => 2
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Category_Control(
			$wp_customize,
			'featured_cat',
			array(
				'label'    => 'Select Featured Category',
				'settings' => 'xeom_featured_cat',
				'section'  => 'aztheme_new_section_featured',
				'priority'	 => 3
			)
		)
	);	
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'featured_id',
			array(
				'label'      => 'Select featured post/page IDs',
				'section'    => 'aztheme_new_section_featured',
				'settings'   => 'xeom_featured_id',
				'type'		 => 'text',
				'priority'	 => 4
			)
		)
	);
	
	$wp_customize->add_control(
		new Customize_Number_Control(
			$wp_customize,
			'featured_slider_slides',
			array(
				'label'      => 'Amount of Slides',
				'section'    => 'aztheme_new_section_featured',
				'settings'   => 'xeom_featured_slider_slides',
				'type'		 => 'number',
				'priority'	 => 5
			)
		)
	);
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'featured_slider_auto_play',
			array(
				'label'      => 'Autoplay ?',
				'section'    => 'aztheme_new_section_featured',
				'settings'   => 'xeom_featured_slider_auto_play',
				'type'		 => 'checkbox',
				'priority'	 => 6
			)
		)
	);
    
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'featured_slider_autoplayTimeout',
			array(
				'label'      => 'Autoplay Timeout',
				'section'    => 'aztheme_new_section_featured',
				'settings'   => 'xeom_featured_slider_autoplayTimeout',
				'type'		 => 'text',
				'priority'	 => 7
			)
		)
	);    
    
    // Social Media
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'facebook',
			array(
				'label'      => 'Facebook',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_facebook',
				'type'		 => 'text',
				'priority'	 => 1
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'twitter',
			array(
				'label'      => 'Twitter',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_twitter',
				'type'		 => 'text',
				'priority'	 => 2
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'instagram',
			array(
				'label'      => 'Instagram',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_instagram',
				'type'		 => 'text',
				'priority'	 => 3
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'pinterest',
			array(
				'label'      => 'Pinterest',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_pinterest',
				'type'		 => 'text',
				'priority'	 => 4
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'bloglovin',
			array(
				'label'      => 'Bloglovin',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_bloglovin',
				'type'		 => 'text',
				'priority'	 => 5
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'google',
			array(
				'label'      => 'Google Plus',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_google',
				'type'		 => 'text',
				'priority'	 => 6
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'tumblr',
			array(
				'label'      => 'Tumblr',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_tumblr',
				'type'		 => 'text',
				'priority'	 => 7
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'youtube',
			array(
				'label'      => 'Youtube',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_youtube',
				'type'		 => 'text',
				'priority'	 => 8
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'dribbble',
			array(
				'label'      => 'Dribbble',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_dribbble',
				'type'		 => 'text',
				'priority'	 => 9
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'soundcloud',
			array(
				'label'      => 'Soundcloud',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_soundcloud',
				'type'		 => 'text',
				'priority'	 => 10
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'vimeo',
			array(
				'label'      => 'Vimeo',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_vimeo',
				'type'		 => 'text',
				'priority'	 => 11
			)
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'linkedin',
			array(
				'label'      => 'Linkedin (Use full URL to your Linkedin profile)',
				'section'    => 'aztheme_new_section_social_media',
				'settings'   => 'xeom_linkedin',
				'type'		 => 'text',
				'priority'	 => 12
			)
		)
	);
	// Accent
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'accent_color',
			array(
				'label'      => 'Accent Color',
				'section'    => 'aztheme_new_section_color_accent',
				'settings'   => 'xeom_accent_color',
				'priority'	 => 1
			)
		)
	);
    // Footer
    $wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'footer_copyright',
			array(
				'label'      => 'Footer copyright text',
				'section'    => 'aztheme_new_section_footer',
				'settings'   => 'xeom_footer_copyright',
				'type'		 => 'text',
				'priority'	 => 2
			)
		)
	);
    // Custom CSS
	$wp_customize->add_control(
		new Customize_CustomCss_Control(
			$wp_customize,
			'custom_css',
			array(
				'label'      => 'Custom CSS',
				'section'    => 'aztheme_new_section_custom_css',
				'settings'   => 'xeom_custom_css',
				'type'		 => 'custom_css',
				'priority'	 => 1
			)
		)
	);
}
add_action( 'customize_register', 'aztheme_register_theme_customizer' );