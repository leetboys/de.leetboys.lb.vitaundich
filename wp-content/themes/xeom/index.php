<?php
    get_header();    
    $xeom_blog_layout = get_theme_mod('xeom_homepage_layout', true);
    if ( isset( $_GET['blog_layout'] ) ) {
        $blog_layout = trim( $_GET['blog_layout'] );
        if ( in_array( $blog_layout, array( 'standard', 'list', 'zigzag', 'masonry' ) ) ) {
            $xeom_blog_layout = $blog_layout;
        }
    }
    $xeom_disable_homepage_sidebar = get_theme_mod('xeom_disable_homepage_sidebar', true);
    if ( $xeom_disable_homepage_sidebar ) {
        $xeom_col_md = 12;
    } else {
        $xeom_col_md = 9;
    }
    
    if ( isset($_GET['sidebar']) && trim($_GET['sidebar']) == 'no' ) {
        $xeom_col_md = 12;
    }
?>
<div class="container">
    <div class="row">
        <div class="col-md-<?php echo esc_attr($xeom_col_md); ?>">
            <?php if ( $xeom_blog_layout == 'list' ) : ?>
                <?php get_template_part('loop/blog', 'list'); ?>
            <?php elseif ( $xeom_blog_layout == 'masonry' ) : ?>
                <?php get_template_part('loop/blog', 'masonry'); ?>
            <?php elseif ( $xeom_blog_layout == 'zigzag' ) : ?>
                <?php get_template_part('loop/blog', 'zigzag'); ?>
            <?php else : ?>
                <?php get_template_part('loop/blog', 'standard'); ?>
            <?php endif; ?>
        </div>
        <?php if ( !$xeom_disable_homepage_sidebar ) : ?>
        <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>