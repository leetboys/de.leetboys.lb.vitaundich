<?php
    get_header();
    $xeom_disable_single_post_sidebar = get_theme_mod('xeom_disable_single_post_sidebar', false);
    if  ( $xeom_disable_single_post_sidebar == true ) {
        $xeom_col_md = 12;
    } else {
        $xeom_col_md = 9;
    }
?>
<div class="container">
    <div class="row">
        <div class="col-md-<?php echo esc_attr($xeom_col_md); ?>">
            <div id="main">
                <?php while ( have_posts() ) : the_post(); ?>
                <article <?php post_class('post az-single-post'); ?>>
                    <?php get_template_part('template-parts/post', 'format'); ?>
                    <div class="post-content">
                        <p class="post-cats"><i class="fa fa-bookmark-o"></i>&nbsp;&nbsp;<?php the_category(', '); ?></p>
                        <?php if ( get_the_title() ) : ?>
                        <h4 class="post-title"><?php the_title(); ?></h4>
                        <?php endif; ?>                        
                        <div class="single-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages(); ?>
                        </div>                        
                        <?php if ( get_the_tags() ) : ?>
                        <div class="az-post-tags">
                            <?php the_tags('',' '); ?>
                        </div>                        
                        <?php endif; ?>
                        <div class="single-post-meta">
                            <?php get_template_part('template-parts/post', 'meta'); ?>
                        </div>
                        <!--Previous/Next-Post-->
                        <div class="az-single-post-next-prev">
                            <div class="prev-post"><?php previous_post_link('%link', '<span class="fa fa-angle-left"></span>Vorheriger Artikel'); ?></div>
                            <div class="next-post"><?php next_post_link('%link', 'Nächster Artikel<span class="fa fa-angle-right"></span>'); ?></div>
                        </div>
                        <!--.Previous/Next-Post-->
                        <?php get_template_part( 'template-parts/single', 'post-author' ); ?>
                        <?php get_template_part( 'template-parts/single', 'post-related' ); ?>
                        <?php comments_template( '', true );  ?>
                    </div>
                </article>
                <?php endwhile; ?>
            </div>
        </div>
        <?php if ( !$xeom_disable_single_post_sidebar ) : ?>
        <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>