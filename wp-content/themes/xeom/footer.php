    </section><!-- End #main-container -->
    <footer id="az-footer">
        <div class="container">
            <div class="az-top-footer">
                <div class="row">
                    <div class="col-md-4"><?php if ( is_active_sidebar('footer-1') ) { dynamic_sidebar('footer-1'); } ?></div>
                    <div class="col-md-4"><?php if ( is_active_sidebar('footer-2') ) { dynamic_sidebar('footer-2'); } ?></div>
                    <div class="col-md-4"><?php if ( is_active_sidebar('footer-3') ) { dynamic_sidebar('footer-3'); } ?></div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright-inner">
                    <p class="copyright"><?php echo wp_kses_post(get_theme_mod('xeom_footer_copyright', '&copy; 2015 AZ-Theme. All right reserved.')); ?></p>
                    <a class="back-to-top" href="#"><i class="fa fa-angle-up"></i> Top</a>
                </div>
            </div>
        </div>        
    </footer>
    <?php wp_footer(); ?>
</body>
</html>