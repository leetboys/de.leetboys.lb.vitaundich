<?php
    get_header();    
    $xeom_archive_layout = get_theme_mod('xeom_archive_layout', true);
    $xeom_disable_archive_sidebar = get_theme_mod('xeom_disable_archive_sidebar', true);
    if ( $xeom_disable_archive_sidebar ) {
        $xeom_col_md = 12;
    } else {
        $xeom_col_md = 9;
    }
?>
<div class="container">
    <div class="archive-box">
		<span><?php esc_html_e( 'Ergebnis für', 'xeom' ); ?>:&nbsp;</span>
		<h3><?php printf( __( '%s', 'xeom' ), get_search_query() ); ?></h3>
	</div>
    <div class="row">
        <div class="col-md-<?php echo esc_attr($xeom_col_md); ?>">
            <?php if ( $xeom_archive_layout == 'list' ) : ?>
                <?php get_template_part('loop/blog', 'list'); ?>
            <?php elseif ( $xeom_archive_layout == 'masonry' ) : ?>
                <?php get_template_part('loop/blog', 'masonry'); ?>
            <?php elseif ( $xeom_archive_layout == 'zigzag' ) : ?>
                <?php get_template_part('loop/blog', 'zigzag'); ?>
            <?php else : ?>
                <?php get_template_part('loop/blog', 'standard'); ?>
            <?php endif; ?>
        </div>
        <?php if ( !$xeom_disable_archive_sidebar ) : ?>
        <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>