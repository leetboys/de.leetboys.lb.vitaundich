<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <menu id="main-menu"<?php if ( !get_theme_mod('xeom_disable_is_sticky_menu') ) : ?>class="is_ticky_nav"<?php endif; ?>>
        <div class="container">
            <div class="az-menu-top">
                <a href="javascript:void(0)" class="togole-mainmenu"><i class="fa fa-bars"></i></a>
                <div id="nav-wrapper">
                    <?php
                        wp_nav_menu( array (
                            'container' => false,
                            'theme_location' => 'primary',
                            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                            'depth' => 10,
                            'walker' => new wp_bootstrap_navwalker()
                        ) );
                    ?>
                    <div id="az-mobile-menu"></div>
                </div>
                <?php if ( !get_theme_mod('xeom_disable_search_menu') ) : ?>
                <div class="top-search">                    
                    <a href="javascript:void(0)" class="icon-search"><i class="fa fa-search"></i></a>
                    <div class="az-search">
                    	<div class="inner-search">
                    		<?php get_search_form(); ?>
                    	</div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( !get_theme_mod('xeom_disable_top_social') ) : ?>
                <div class="socials<?php if ( get_theme_mod('xeom_disable_search_menu') ) : ?> no-top-search<?php endif; ?>">
                    <?php if(get_theme_mod('xeom_facebook')) : ?><a class="social-icon" href="http://facebook.com/<?php echo esc_html(get_theme_mod('xeom_facebook')); ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_twitter')) : ?><a class="social-icon" href="http://twitter.com/<?php echo esc_html(get_theme_mod('xeom_twitter')); ?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_instagram')) : ?><a class="social-icon" href="http://instagram.com/<?php echo esc_html(get_theme_mod('xeom_instagram')); ?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_pinterest')) : ?><a class="social-icon" href="http://pinterest.com/<?php echo esc_html(get_theme_mod('xeom_pinterest')); ?>" target="_blank"><i class="fa fa-pinterest"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_bloglovin')) : ?><a class="social-icon" href="http://bloglovin.com/<?php echo esc_html(get_theme_mod('xeom_bloglovin')); ?>" target="_blank"><i class="fa fa-heart"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_google')) : ?><a class="social-icon" href="http://plus.google.com/<?php echo esc_html(get_theme_mod('xeom_google')); ?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_tumblr')) : ?><a class="social-icon" href="http://<?php echo esc_html(get_theme_mod('xeom_tumblr')); ?>.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_youtube')) : ?><a class="social-icon" href="http://youtube.com/<?php echo esc_html(get_theme_mod('xeom_youtube')); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_dribbble')) : ?><a class="social-icon" href="http://dribbble.com/<?php echo esc_html(get_theme_mod('xeom_dribbble')); ?>" target="_blank"><i class="fa fa-dribbble"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_soundcloud')) : ?><a class="social-icon" href="http://soundcloud.com/<?php echo esc_html(get_theme_mod('xeom_soundcloud')); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_vimeo')) : ?><a class="social-icon" href="http://vimeo.com/<?php echo esc_html(get_theme_mod('xeom_vimeo')); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a><?php endif; ?>
    				<?php if(get_theme_mod('xeom_linkedin')) : ?><a class="social-icon" href="<?php echo esc_html(get_theme_mod('xeom_linkedin')); ?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </menu>
    <header class="az-header-wrapper">        
        <div class="container">
            <div id="logo">
            <?php if(!get_theme_mod('xeom_logo')) : ?>
                <?php if(is_front_page()) : ?>
                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() .'/assets/images/logo.png' ); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
                <?php else : ?>
                    <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() .'/assets/images/logo.png' ); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h2>
                <?php endif; ?>
            <?php else : ?>
                <?php if(is_front_page()) : ?>
                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_theme_mod('xeom_logo')); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
                <?php else : ?>
                    <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_theme_mod('xeom_logo')); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h2>
                <?php endif; ?>
            <?php endif; ?>
            </div>
        </div>        
    </header>
    <?php if(get_theme_mod( 'xeom_featured_slider' ) == true && !is_404() && is_front_page() ) : ?>
		<?php get_template_part('template-parts/featured', 'slider'); ?>
	<?php endif; ?>
    <section id="main-container">
    <?php
        $xeom_blog_layout = get_theme_mod('xeom_homepage_layout', true);
        $xeom_archive_layout = get_theme_mod('xeom_archive_layout', true);
        $xeom_disable_homepage_sidebar = get_theme_mod('xeom_disable_homepage_sidebar', true);
        $xeom_disable_archive_sidebar = get_theme_mod('xeom_disable_archive_sidebar', true);
        $xeom_disable_single_post_sidebar = get_theme_mod('xeom_disable_single_post_sidebar', true);
        
        if ( isset( $_GET['blog_layout'] ) ) {
            $blog_layout = trim( $_GET['blog_layout'] );
            if ( in_array( $blog_layout, array( 'standard', 'list', 'zigzag', 'masonry' ) ) ) {
                $xeom_blog_layout = $blog_layout;
            }
        }
        
        $xeom_sidebar = ( isset($_GET['sidebar']) && trim($_GET['sidebar']) == 'no' ) ? false : true;
        if ( $xeom_sidebar ) {
            $xeom_col_md = 9;
        } else {
            $xeom_col_md = 12;
        }        
        $GLOBALS['xeom_blog_layout'] = $xeom_blog_layout;
        $GLOBALS['xeom_sidebar'] = $xeom_sidebar;
        $GLOBALS['xeom_col_md'] = $xeom_col_md;
    ?>