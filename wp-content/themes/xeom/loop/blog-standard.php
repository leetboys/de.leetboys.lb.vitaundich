<?php
    $xeom_post_excerpt = 250;
    if ( is_front_page() && get_theme_mod('xeom_disable_homepage_sidebar', true) ) {
        $xeom_post_excerpt = 400;
    } elseif ( ( is_search() || is_archive() ) && get_theme_mod('xeom_disable_archive_sidebar', true) ) {
        $xeom_post_excerpt = 400;
    }
?>
<div class="az-blog az-blog-standard">                    
    <?php if ( have_posts() ) : $xeom_count = 0; ?>
        <?php while ( have_posts() ) : the_post(); $xeom_count++; ?>
            <?php $sticky_class = ( is_sticky() ) ? 'is_sticky' : null; ?>
            <article <?php post_class("post {$sticky_class}"); ?>>
                <?php if ( has_post_format('gallery') ) : ?>
                    <?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true ); ?>
                    <?php if ( $images ) : ?>
                    <div class="post-format post-gallery slider">
                        <?php foreach ( $images as $image_id ) : ?>
                            <?php                
                                $image_url = xeom_resize_image( $image_id, null, 1170, 780, true, true );
                                $image_url = $image_url['url'];
                                $imgage_caption = get_post_field( 'post_excerpt', $image_id );
                            ?>
                			<div class="slide-item"><img alt="" src="<?php echo esc_url($image_url); ?>" <?php if ($imgage_caption) : ?>title="<?php echo esc_attr($imgage_caption); ?>"<?php endif; ?> /></div>			
                		<?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                <?php else : ?>
                    <div class="post-format">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('xeom-fullwidth'); ?>
                        </a>
                    </div>
                <?php endif; ?>
                <div class="post-content">
                    <p class="post-cats"><i class="fa fa-bookmark-o"></i>&nbsp;&nbsp;<?php the_category(', '); ?></p>
                    <?php if ( get_the_title() ) : ?>
                    <h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <?php endif; ?>
                    <div class="post-excerpt">
                        <?php xeom_the_excerpt_max_charlength($xeom_post_excerpt); ?>
                        <?php wp_link_pages(); ?>
                    </div>
                    <?php get_template_part('template-parts/post', 'meta'); ?>
                </div>
            </article>
        <?php endwhile; ?>
    <?php else: ?>
        <?php get_template_part('template-parts/post', 'none'); ?>
    <?php endif; ?>
</div>
<!-- Pagination -->
<?php xeom_pagination(); ?>
<!-- End / Pagination -->