<?php
define('XEOM_LIBS_URI', get_template_directory_uri() . '/libs/');
define('XEOM_CORE_PATH', get_template_directory() . '/core/');
define('XEOM_CORE_URI', get_template_directory_uri() . '/core/');
define('XEOM_CORE_CLASSES', XEOM_CORE_PATH . 'classes/');
define('XEOM_CORE_FUNCTIONS', XEOM_CORE_PATH . 'functions/');
define('XEOM_CORE_WIDGETS', XEOM_CORE_PATH . 'widgets/');

// Set Content Width
if ( ! isset( $content_width ) ) { $content_width = 1170; }

// Theme setup
add_action('after_setup_theme', 'xeom_setup');
function xeom_setup()
{
    load_theme_textdomain( 'xeom', get_template_directory() . '/languages' );
	add_theme_support('title-tag');
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');
	add_image_size('xeom-fullwidth', 1170, 0, true);
	register_nav_menus(
        array(
            'primary' => esc_html__('Primary menu', 'xeom')
        )
    );
	add_theme_support('post-formats', array('image', 'video', 'audio', 'gallery'));
}

// Load Google fonts
function xeom_google_fonts_url()
{
    $fonts_url = '';
    $Roboto = _x( 'on', 'Roboto font: on or off', 'xeom' );
    $Raleway = _x( 'on', 'Montserrat font: on or off', 'xeom' );    

    if ( 'off' !== $Roboto || 'off' !== $Raleway )
    {
        $font_families = array();

        if ('off' !== $Roboto) {
            $font_families[] = 'Roboto:300,400,500,700,900';
        }
        
        if ('off' !== $Raleway) {
            $font_families[] = 'Raleway:400,500,600,700';
        }

        $query_args = array(
            'family' => urlencode(implode('|', $font_families )),
            'subset' => urlencode('latin,latin-ext')
        );

        $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
    }

    return esc_url_raw($fonts_url);
}

// Google fonts
function xeom_enqueue_googlefonts() {
    wp_enqueue_style( 'xeom-googlefonts', xeom_google_fonts_url(), array(), null );
}
add_action('wp_enqueue_scripts', 'xeom_enqueue_googlefonts');

// Register & Enqueue Styles / Scripts
add_action('wp_enqueue_scripts', 'xeom_load_scripts');
function xeom_load_scripts()
{
    // CSS
    wp_enqueue_style('bootstrap', XEOM_LIBS_URI . 'bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('font-awesome', XEOM_LIBS_URI . 'font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('chosen-min', XEOM_LIBS_URI . 'chosen/chosen.min.css');
    wp_enqueue_style('owl-carousel', XEOM_LIBS_URI . 'owl/owl.carousel.css');
    wp_enqueue_style('xeom-style', get_stylesheet_directory_uri() . '/style.css');

    // JS
	wp_enqueue_script('fitvids', XEOM_LIBS_URI . 'fitvids/fitvids.js', array(), false, true);
    wp_enqueue_script('owl-carousel', XEOM_LIBS_URI . 'owl/owl.carousel.min.js', array(), false, true);
    wp_enqueue_script('az-masonry', XEOM_LIBS_URI . 'masonry/masonry.min.js', array(), false, true);
    wp_enqueue_script('chosen', XEOM_LIBS_URI . 'chosen/chosen.jquery.min.js', array(), false, true);
    wp_enqueue_script('xeom-scripts', get_template_directory_uri() . '/assets/js/xeom-scripts.js', array(), false, true);
    
    if ( is_singular() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script('comment-reply');
    }
}

// Author Social Links
add_filter( 'user_contactmethods', 'xeom_author_social_links' );
function xeom_author_social_links()
{
    $contactmethods              = array();
	$contactmethods['twitter']   = 'Twitter Username';
	$contactmethods['facebook']  = 'Facebook Username';
	$contactmethods['google']    = 'Google Plus Username';
	$contactmethods['tumblr']    = 'Tumblr Username';
	$contactmethods['instagram'] = 'Instagram Username';
	$contactmethods['pinterest'] = 'Pinterest Username';
	return $contactmethods;
}

// Display custom categories
function xeom_display_custom_categories( $post_id, $limit = 5 )
{
    $cats = array(); $i = 0; $post_id = (int)$post_id;
    foreach( wp_get_post_categories($post_id) as $c )
    {
        $i++;
        if ( $i <= $limit )
        {
            $cat = get_category($c);
            array_push($cats, '<a href="'.(get_category_link($cat->term_id)).'">'.esc_html($cat->name).'</a>');
        }
        
        if ( $i == $limit + 1 ) {
            array_push($cats, '...');
        }
    }
    
    if ( sizeOf($cats) > 0 ) {
    	$post_categories = implode(', ',$cats);
    } else {
    	$post_categories = "Not Assigned";
    }
    
    return $post_categories;
}

// Register Sidebar
if ( function_exists('register_sidebar') )
{
	register_sidebar(array(
		'name'            => 'Sidebar',
		'id'              => 'sidebar',
		'before_widget'   => '<div id="%1$s" class="widget %2$s">',
		'after_widget'    => '</div>',
		'before_title'    => '<h4 class="widget-title">',
		'after_title'     => '</h4>'
	));
    register_sidebar(array(
		'name'            => 'Footer 1',
		'id'              => 'footer-1',
		'before_widget'   => '<div id="%1$s" class="widget %2$s">',
		'after_widget'    => '</div>',
		'before_title'    => '<h4 class="widget-title">',
		'after_title'     => '</h4>'
	));
    register_sidebar(array(
		'name'            => 'Footer 2',
		'id'              => 'footer-2',
		'before_widget'   => '<div id="%1$s" class="widget %2$s">',
		'after_widget'    => '</div>',
		'before_title'    => '<h4 class="widget-title">',
		'after_title'     => '</h4>'
	));
    register_sidebar(array(
		'name'            => 'Footer 3',
		'id'              => 'footer-3',
		'before_widget'   => '<div id="%1$s" class="widget %2$s">',
		'after_widget'    => '</div>',
		'before_title'    => '<h4 class="widget-title">',
		'after_title'     => '</h4>'
	));
}

function xeom_require_file( $path ) {
    if ( file_exists($path) ) {
        require $path;
    }
}

// Require core files
xeom_require_file( get_template_directory() . '/core/init.php' );

// Comment Layout
function xeom_custom_comment($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	} ?>
	<<?php echo esc_attr($tag); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
		<div class="comment-author">
		<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
		</div>
		<div class="comment-content">
			<?php printf( __( '<h4 class="author-name">%s</h4>', 'xeom' ), get_comment_author_link() ); ?>
			<span class="date-comment">
				<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php printf( __('%1$s at %2$s', 'xeom'), get_comment_date(),  get_comment_time() ); ?></a>
			</span>
			<div class="reply">
				<?php edit_comment_link( esc_html__( '(Edit)', 'xeom' ), '  ', '' );?>
				<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'xeom' ); ?></em>
				<br />
			<?php endif; ?>
			<div class="comment-text"><?php comment_text(); ?></div>
		</div>	
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php
}

//Pagination
function xeom_pagination()
{
    global $wp_query;
    if ( (int)$wp_query->found_posts > (int)get_option('posts_per_page') ) :
    ?>
    <div class="az-pagination">
        <div class="row">
            <div class="older col-xs-6 col-md-6"><?php next_posts_link('<i class="fa fa-angle-left"></i> Older Posts'); ?></div>
            <div class="newer col-xs-6 col-md-6"><?php previous_posts_link('Newer Posts <i class="fa fa-angle-right"></i>'); ?></div>
        </div>
	</div>
    <?php
    endif;
}

// The Excerpt
function xeom_custom_excerpt_length($length) { return 1000; }
add_filter( 'excerpt_length', 'xeom_custom_excerpt_length', 999 );

// Custom excerpt max charlength
function xeom_the_excerpt_max_charlength($charlength)
{
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '&nbsp;...';
	} else {
		echo $excerpt;
	}
}

// Url Encode
function xeom_url_encode($title)
{
    $title = html_entity_decode($title);
    $title = urlencode($title);
    return $title;
}

// Include the TGM_Plugin_Activation class
add_action('tgmpa_register', 'xeom_register_required_plugins');
function xeom_register_required_plugins()
{
	$plugins = array(
		array(
			'name'     				=> 'Vafpress Post Formats UI',
			'slug'     				=> 'vafpress-post-formats-ui-develop',
			'source'   				=> 'http://resources.az-theme.net/vafpress-post-formats-ui-develop.zip',
			'required' 				=> true,
			'version' 				=> '1.5',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> ''
		),
        array(
			'name'     				=> 'WP Instagram Widget',
			'slug'     				=> 'wp-instagram-widget',
			'required' 				=> false,
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> ''
		),
        array(
			'name'     				=> 'Categories Images',
			'slug'     				=> 'categories-imagest',
			'source' 			    => 'https://downloads.wordpress.org/plugin/categories-images.2.5.2.zip'
		),
		array(
			'name'     				=> 'Contact Form 7',
			'slug'     				=> 'contact-form-7',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> ''
		),
        array(
			'name'     				=> 'MailChimp for WordPress Lite',
			'slug'     				=> 'mailchimp-for-wp',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> ''
		)
	);

	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                    // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to out
	);
	tgmpa($plugins, $config);
}